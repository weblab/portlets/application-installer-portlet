# WebLab Application Installer Portlet

This project is in charge of creating the pages/tabs/layouts to be used inside Liferay Portal for a WebLab application.
It enables to define this pages and the portlets that should be deployed on it.

This portlet can also be instantiated in order to create user for the application (however it cannot be instantiated by it self!).
The super administrator also has the ability (when instantiated) to recreate the application pages by clicking on a button.


# Build status
[![build status](https://gitlab.ow2.org/weblab/portlets/application-installer-portlet/badges/master/build.svg)](https://gitlab.ow2.org/weblab/portlets/application-installer-portlet/commits/master)

# How to build it

In order to be build it, you need to have access to the Maven dependencies it is using. Most of the dependencies are in the central repository and thus does not implies specific configuration.
However, the WebLab Core dependencies are not yet included in the Maven central repository but in a dedicated one that we manage ourselves.
Thus you may have to add the repositories that are listed in the settings.xml. 
