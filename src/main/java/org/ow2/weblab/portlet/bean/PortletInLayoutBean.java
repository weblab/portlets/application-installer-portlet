/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean;

import java.util.Map;

/**
 * To describe portlets in a layout
 * 
 * @author emilienbondu
 *
 */
public class PortletInLayoutBean {


	private String portletId, column;


	private Map<String, String> preferences;


	/**
	 * @return the portletId
	 */
	public String getPortletId() {
		return this.portletId;
	}


	/**
	 * @param portletId
	 *            the portletId to set
	 */
	public void setPortletId(final String portletId) {
		this.portletId = portletId;
	}


	/**
	 * @return the column
	 */
	public String getColumn() {
		return this.column;
	}


	/**
	 * @param column
	 *            the column to set
	 */
	public void setColumn(final String column) {
		this.column = column;
	}


	/**
	 * @return the preferences
	 */
	public Map<String, String> getPreferences() {
		return this.preferences;
	}


	/**
	 * @param preferences
	 *            the preferences to set
	 */
	public void setPreferences(final Map<String, String> preferences) {
		this.preferences = preferences;
	}

}
