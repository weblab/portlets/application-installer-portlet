/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean;

import java.util.List;

/**
 * To describe an application (layouts, users, roles, permissions).
 *
 * @author emilienbondu, ymombrun
 */
public class ApplicationBean {


	private String name = "WebLab", defaultThemeId;


	private List<LayoutBean> layouts;


	private List<RoleBean> roles;


	private List<LayoutRolePermissionBean> layoutsRolesPermissions;


	private List<UserBean> users;


	private List<JournalArticleBean> journalArticles;


	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}


	/**
	 * @return the defaultThemeId
	 */
	public String getDefaultThemeId() {
		return this.defaultThemeId;
	}


	/**
	 * @param defaultThemeId
	 *            the defaultThemeId to set
	 */
	public void setDefaultThemeId(final String defaultThemeId) {
		this.defaultThemeId = defaultThemeId;
	}


	/**
	 * @return the layouts
	 */
	public List<LayoutBean> getLayouts() {
		return this.layouts;
	}


	/**
	 * @param layouts
	 *            the layouts to set
	 */
	public void setLayouts(final List<LayoutBean> layouts) {
		this.layouts = layouts;
	}


	/**
	 * @return the roles
	 */
	public List<RoleBean> getRoles() {
		return this.roles;
	}


	/**
	 * @param roles
	 *            the roles to set
	 */
	public void setRoles(final List<RoleBean> roles) {
		this.roles = roles;
	}


	/**
	 * @return the layoutsRolesPermissions
	 */
	public List<LayoutRolePermissionBean> getLayoutsRolesPermissions() {
		return this.layoutsRolesPermissions;
	}


	/**
	 * @param layoutsRolesPermissions
	 *            the layoutsRolesPermissions to set
	 */
	public void setLayoutsRolesPermissions(final List<LayoutRolePermissionBean> layoutsRolesPermissions) {
		this.layoutsRolesPermissions = layoutsRolesPermissions;
	}


	/**
	 * @return the users
	 */
	public List<UserBean> getUsers() {
		return this.users;
	}


	/**
	 * @param users
	 *            the users to set
	 */
	public void setUsers(final List<UserBean> users) {
		this.users = users;
	}


	/**
	 * @return the journalArticles
	 */
	public List<JournalArticleBean> getJournalArticles() {
		return this.journalArticles;
	}


	/**
	 * @param journalArticles
	 *            the journalArticles to set
	 */
	public void setJournalArticles(final List<JournalArticleBean> journalArticles) {
		this.journalArticles = journalArticles;
	}

}
