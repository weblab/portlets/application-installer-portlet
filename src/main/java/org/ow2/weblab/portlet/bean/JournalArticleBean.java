/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean;

import java.util.Map;

/**
 * To describe a journal content
 *
 * @author emilienbondu
 */
public class JournalArticleBean {


	private String name;


	private boolean privateContent;


	private Map<String, String> description, title, content;


	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}


	/**
	 * @return the privateContent
	 */
	public boolean isPrivateContent() {
		return this.privateContent;
	}


	/**
	 * @param privateContent
	 *            the privateContent to set
	 */
	public void setPrivateContent(final boolean privateContent) {
		this.privateContent = privateContent;
	}


	/**
	 * @return the description
	 */
	public Map<String, String> getDescription() {
		return this.description;
	}


	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final Map<String, String> description) {
		this.description = description;
	}


	/**
	 * @return the title
	 */
	public Map<String, String> getTitle() {
		return this.title;
	}


	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(final Map<String, String> title) {
		this.title = title;
	}


	/**
	 * @return the content
	 */
	public Map<String, String> getContent() {
		return this.content;
	}


	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(final Map<String, String> content) {
		this.content = content;
	}

}
