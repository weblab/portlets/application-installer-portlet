/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean;

/**
 * To describe permissions associated to role for a given layout.
 * 
 * @author emilienbondu
 *
 */
public class LayoutRolePermissionBean {


	private String role, layout;


	private String[] actions;


	/**
	 * @return the role
	 */
	public String getRole() {
		return this.role;
	}


	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(final String role) {
		this.role = role;
	}


	/**
	 * @return the layout
	 */
	public String getLayout() {
		return this.layout;
	}


	/**
	 * @param layout
	 *            the layout to set
	 */
	public void setLayout(final String layout) {
		this.layout = layout;
	}


	/**
	 * @return the actions
	 */
	public String[] getActions() {
		return this.actions;
	}


	/**
	 * @param actions
	 *            the actions to set
	 */
	public void setActions(final String[] actions) {
		this.actions = actions;
	}


}
