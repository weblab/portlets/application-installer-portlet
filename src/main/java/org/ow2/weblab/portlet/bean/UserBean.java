/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean;

import java.util.List;

/**
 * To describe an user.
 * 
 * @author emilienbondu
 *
 */
public class UserBean {


	private String firstName, lastName, email;


	private List<String> roles;


	private boolean organizationOwner;


	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return this.firstName;
	}


	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}


	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return this.lastName;
	}


	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return this.email;
	}


	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(final String email) {
		this.email = email;
	}


	/**
	 * @return the roles
	 */
	public List<String> getRoles() {
		return this.roles;
	}


	/**
	 * @param roles
	 *            the roles to set
	 */
	public void setRoles(final List<String> roles) {
		this.roles = roles;
	}


	/**
	 * @return the organizationOwner
	 */
	public boolean isOrganizationOwner() {
		return this.organizationOwner;
	}


	/**
	 * @param organizationOwner
	 *            the organizationOwner to set
	 */
	public void setOrganizationOwner(final boolean organizationOwner) {
		this.organizationOwner = organizationOwner;
	}

}
