/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.bean;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * To describe a layout
 *
 * @author emilienbondu, ymombrun
 */
public class LayoutBean {


	private String name, friendlyURL, layoutTemplateId;


	private boolean privateLayout, hidden;


	private List<PortletInLayoutBean> portlets = new LinkedList<>();


	private List<LayoutBean> innerLayouts = new LinkedList<>();


	private Map<String, String> description = new HashMap<>();


	private Map<String, String> title = new HashMap<>();


	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}


	/**
	 * @return the friendlyURL
	 */
	public String getFriendlyURL() {
		return this.friendlyURL;
	}


	/**
	 * @param friendlyURL
	 *            the friendlyURL to set
	 */
	public void setFriendlyURL(final String friendlyURL) {
		this.friendlyURL = friendlyURL;
	}


	/**
	 * @return the layoutTemplateId
	 */
	public String getLayoutTemplateId() {
		return this.layoutTemplateId;
	}


	/**
	 * @param layoutTemplateId
	 *            the layoutTemplateId to set
	 */
	public void setLayoutTemplateId(final String layoutTemplateId) {
		this.layoutTemplateId = layoutTemplateId;
	}


	/**
	 * @return the privateLayout
	 */
	public boolean isPrivateLayout() {
		return this.privateLayout;
	}


	/**
	 * @param privateLayout
	 *            the privateLayout to set
	 */
	public void setPrivateLayout(final boolean privateLayout) {
		this.privateLayout = privateLayout;
	}


	/**
	 * @return the hidden
	 */
	public boolean isHidden() {
		return this.hidden;
	}


	/**
	 * @param hidden
	 *            the hidden to set
	 */
	public void setHidden(final boolean hidden) {
		this.hidden = hidden;
	}


	/**
	 * @return the portlets
	 */
	public List<PortletInLayoutBean> getPortlets() {
		return this.portlets;
	}


	/**
	 * @param portlets
	 *            the portlets to set
	 */
	public void setPortlets(final List<PortletInLayoutBean> portlets) {
		this.portlets = portlets;
	}


	/**
	 * @return the description
	 */
	public Map<String, String> getDescription() {
		return this.description;
	}


	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final Map<String, String> description) {
		this.description = description;
	}


	/**
	 * @return the title
	 */
	public Map<String, String> getTitle() {
		return this.title;
	}


	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(final Map<String, String> title) {
		this.title = title;
	}


	/**
	 * @return the innerLayouts
	 */
	public List<LayoutBean> getInnerLayouts() {
		return this.innerLayouts;
	}


	/**
	 * @param innerLayouts
	 *            the innerLayouts to set
	 */
	public void setInnerLayouts(final List<LayoutBean> innerLayouts) {
		this.innerLayouts = innerLayouts;
	}

}
