/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.mvc.spring.validator;

import org.ow2.weblab.portlet.bean.UserBean;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;



/**
 */
public class UserValidator implements Validator {


	@Override
	public boolean supports(Class<?> clazz) {
		boolean res = UserBean.class.equals(clazz);
		return res;
	}


	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "field.required", "field.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "field.required", "field.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "field.required", "field.required");

		String email = ((UserBean) target).getEmail();

		if (!email.isEmpty()) {
			if (!(email.contains("@") && email.contains("."))) {
				errors.rejectValue("email", "email.invalid");
			} else {
				try {
					if (UserLocalServiceUtil.fetchUserByEmailAddress(PortalUtil.getDefaultCompanyId(), email) != null) {
						errors.rejectValue("email", "email.exists");
					}
				} catch (@SuppressWarnings("unused") SystemException e) {
					errors.rejectValue("email", "email.uncheck");
				}
			}
		}
	}

}