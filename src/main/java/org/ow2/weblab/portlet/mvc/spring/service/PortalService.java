/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.mvc.spring.service;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.portlet.PortletPreferences;

import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.portlet.bean.JournalArticleBean;
import org.ow2.weblab.portlet.bean.LayoutBean;
import org.springframework.stereotype.Service;

import com.liferay.portal.LayoutFriendlyURLException;
import com.liferay.portal.LayoutFriendlyURLsException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.GroupConstants;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.LayoutConstants;
import com.liferay.portal.model.LayoutTypePortlet;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Portlet;
import com.liferay.portal.model.PortletConstants;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.PortletLocalServiceUtil;
import com.liferay.portal.service.PortletPreferencesLocalServiceUtil;
import com.liferay.portal.service.ResourceLocalServiceUtil;
import com.liferay.portal.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.permission.PortletPermissionUtil;
import com.liferay.portal.util.PortletKeys;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.model.JournalArticleConstants;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;

/**
 * To setup a Liferay portal
 *
 * @author emilienbondu, ymombrun
 */
@Service
public class PortalService {


	/**
	 * The logger used inside
	 */
	protected final Log logger = LogFactory.getLog(this.getClass());


	/**
	 * The default locale to be used for articles (if any) that will be displayed (if the user has configured the portal to be dipslayed in a language where the article is not available).
	 */
	protected final Locale defaultLocale;


	/**
	 * The default constructor using en_US
	 */
	public PortalService() {
		this("en_US");
	}


	/**
	 * @param locale
	 *            The default local to be used when creating user, and configuring layouts
	 */
	public PortalService(final String locale) {
		this.defaultLocale = LocaleUtils.toLocale(locale);
	}


	/**
	 * Add a user
	 *
	 * @param companyId
	 *            The id of the company who employs the user to be created
	 * @param screenName
	 *            The screen name (login) of the user to be created
	 * @param firstName
	 *            The first name of the user to be created
	 * @param lastName
	 *            The last name of the user to be created
	 * @param male
	 *            The gender of the user to be created (<code>true</code> for male, <code>false</code> for female)
	 * @param roleIds
	 *            The ids of the roles of the user to be created
	 * @param applicationName
	 *            The application/organisation name used for email address creation and association to an organisation
	 * @param email
	 *            The email to be used for the user to be created
	 * @return the Liferay user created
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	protected User addUser(final long companyId, final String screenName, final String firstName, final String lastName, final boolean male, final long[] roleIds, final String applicationName,
			final String email) throws Exception {

		final long creatorUserId = 0;
		final boolean autoPassword = false;
		final String password1 = screenName;
		final String password2 = password1;
		final boolean autoScreenName = false;
		final long facebookId = 0;
		final String openId = StringPool.BLANK;
		final String middleName = StringPool.BLANK;
		final int prefixId = 0;
		final int suffixId = 0;
		final int birthdayMonth = Calendar.JANUARY;
		final int birthdayDay = 1;
		final int birthdayYear = 1970;
		final String jobTitle = StringPool.BLANK;

		final Group guestGroup = GroupLocalServiceUtil.getGroup(companyId, GroupConstants.GUEST);
		final long[] groupIds = new long[] { guestGroup.getGroupId() };

		final Organization organization = OrganizationLocalServiceUtil.getOrganization(companyId, applicationName);
		final long[] organizationIds = new long[] { organization.getOrganizationId() };

		final long[] userGroupIds = null;
		final boolean sendEmail = false;

		final ServiceContext serviceContext = new ServiceContext();
		serviceContext.setScopeGroupId(guestGroup.getGroupId());

		User user = UserLocalServiceUtil.fetchUserByEmailAddress(companyId, email);

		if (user != null) {
			UserLocalServiceUtil.deleteUser(user);
		}

		user = UserLocalServiceUtil.addUser(creatorUserId, companyId, autoPassword, password1, password2, autoScreenName, screenName, email, facebookId, openId, this.defaultLocale, firstName,
				middleName, lastName, prefixId, suffixId, male, birthdayMonth, birthdayDay, birthdayYear, jobTitle, groupIds, organizationIds, roleIds, userGroupIds, sendEmail, serviceContext);

		final String question = "My first name is?";
		final String answer = firstName;

		UserLocalServiceUtil.updateReminderQuery(user.getUserId(), question, answer);

		return user;
	}


	/**
	 * Add a layout
	 *
	 * @param group
	 *            The group into which the layout should be added
	 * @param layoutBean
	 *            The layout bean containg description of the layout to be created
	 * @param parentLayoutId
	 *            The id of the parent layout (or the default value)
	 * @return the Liferay layout
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	protected Layout addLayout(final Group group, final LayoutBean layoutBean, final long parentLayoutId) throws Exception {
		this.logger.debug("Try to add layout " + layoutBean.getName());
		final ServiceContext serviceContext = new ServiceContext();

		// adding the layout
		Layout layout = null;
		try {
			layout = LayoutLocalServiceUtil.addLayout(group.getCreatorUserId(), group.getGroupId(), layoutBean.isPrivateLayout(), parentLayoutId, layoutBean.getName(), StringPool.BLANK,
					StringPool.BLANK, LayoutConstants.TYPE_PORTLET, layoutBean.isHidden(), layoutBean.getFriendlyURL(), serviceContext);
		} catch (final LayoutFriendlyURLException e) {
			this.logger.error("Unable to create layout " + layoutBean.getName() + " LayoutFriendlyURLException " + e.getKeywordConflict() + " " + e.getType(), e);
			return null;
		} catch (final LayoutFriendlyURLsException e) {
			StringBuilder sb = new StringBuilder();
			e.getLayoutFriendlyURLExceptions().forEach(inner -> sb.append(inner.getKeywordConflict() + " " + inner.getType()));
			this.logger.error("Unable to create layout " + layoutBean.getName() + " LayoutFriendlyURLsException: " + sb.toString() + ".", e);
			return null;
		} catch (final Exception e) {
			this.logger.error("Unable to create layout " + layoutBean.getName(), e);
			return null;
		}

		// update title / description layout
		final LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
		layoutTypePortlet.setLayoutTemplateId(0, layoutBean.getLayoutTemplateId(), false);

		for (final Entry<String, String> entry : layoutBean.getTitle().entrySet()) {
			layout.setTitle(entry.getValue(), LocaleUtils.toLocale(entry.getKey()), this.defaultLocale);
			layout.setName(entry.getValue(), LocaleUtils.toLocale(entry.getKey()), this.defaultLocale);
			layout = LayoutLocalServiceUtil.updateLayout(layout);
		}
		for (final Entry<String, String> entry : layoutBean.getDescription().entrySet()) {
			layout.setDescription(entry.getValue(), LocaleUtils.toLocale(entry.getKey()), this.defaultLocale);
			layout = LayoutLocalServiceUtil.updateLayout(layout);
		}

		this.logger.debug("Layout " + layoutBean.getName() + " successfully created.");
		return layout;
	}


	/**
	 * Add a portlet to a layout
	 *
	 * @param companyId
	 *            The owner company
	 * @param layout
	 *            The layout into which the portlet shall be added
	 * @param portletId
	 *            The id of the portlet to add
	 * @param columnId
	 *            The column into which the portlet should be added
	 * @param preferences
	 *            A map of pre-configured preferences
	 * @return the portletId
	 */
	protected String addPortletId(final long companyId, final Layout layout, final String portletId, final String columnId, final Map<String, String> preferences) {
		final LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
		this.logger.debug("Trying to add the portlet " + portletId + " on layout " + layout.getName() + ".");
		try {
			final Portlet portlet = PortletLocalServiceUtil.getPortletById(portletId);
			if (portlet.isSystem()) {
				this.logger.warn("This portlet is normally restricted to system. Force it.");
				portlet.setSystem(false);
				PortletLocalServiceUtil.updatePortlet(portlet);
			}

			if (!portletId.equals(portlet.getPortletId())) {
				this.logger.info("The input ID (" + portletId + ") of the portlet is not the same as the retrieved one (" + portlet.getPortletId() + ").");
			}

			this.logger.debug("Still trying to add the portlet " + portlet + " on layout " + layout.getName() + ".");

			final String newPortletId = layoutTypePortlet.addPortletId(PortletKeys.PREFS_OWNER_ID_DEFAULT, portletId, columnId, -1, false);
			if (newPortletId == null || newPortletId.isEmpty()) {
				this.logger.warn("Unable to add the portlet: " + portletId + " on layout " + layout.getName() + ". Result of addition is a null or empty id.");
				// Try with original id otherwise we get an NPE
				PortalService.addPortletToLayout(layout, portletId);
			} else {
				PortalService.addPortletToLayout(layout, newPortletId);
			}

			PortalService.addPortletToLayout(layout, newPortletId);
			LayoutLocalServiceUtil.updateLayout(layout);

			final long ownerId = PortletKeys.PREFS_OWNER_ID_DEFAULT;
			final int ownerType = PortletKeys.PREFS_OWNER_TYPE_LAYOUT;

			// Retrieve the portlet preferences for the journal portlet instance just created
			final PortletPreferences prefs = PortletPreferencesLocalServiceUtil.getPreferences(companyId, ownerId, ownerType, layout.getPlid(), newPortletId);

			if (preferences != null) {
				for (final Entry<String, String> pref : preferences.entrySet()) {
					// set desired article id for content display portlet
					prefs.setValue(pref.getKey(), pref.getValue());
				}

				if (!prefs.getMap().isEmpty()) {
					// update the portlet preferences
					PortletPreferencesLocalServiceUtil.updatePreferences(ownerId, ownerType, layout.getPlid(), newPortletId, prefs);
				}
			}
			return newPortletId;

		} catch (final Exception e) {
			this.logger.warn("Unable to add the portlet: " + portletId + " on layout " + layout.getName() + ".", e);
		}
		return portletId;
	}


	/**
	 * Add a portlet to the layout
	 *
	 * @param layout
	 *            The layout into which the portlet shall be added
	 * @param portletId
	 *            The id of the portlet to add
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	protected static void addPortletToLayout(final Layout layout, final String portletId) throws Exception {
		final String rootPortletId = PortletConstants.getRootPortletId(portletId);
		final String portletPrimaryKey = PortletPermissionUtil.getPrimaryKey(layout.getPlid(), portletId);
		ResourceLocalServiceUtil.addResources(layout.getCompanyId(), layout.getGroupId(), 0, rootPortletId, portletPrimaryKey, true, true, true);
	}


	/**
	 * Add an article from bean
	 *
	 * @param companyId
	 *            The owner company
	 * @param userId
	 *            The author
	 * @param article
	 *            The article description
	 * @return the Liferay article
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	protected JournalArticle addArticle(final long companyId, final long userId, final JournalArticleBean article) throws Exception {
		final ServiceContext serviceContext = new ServiceContext();
		serviceContext.setAddGuestPermissions(true);
		final long groupId = GroupLocalServiceUtil.getGroup(companyId, GroupConstants.GUEST).getGroupId();
		serviceContext.setScopeGroupId(groupId);

		final Map<Locale, String> titleMap = new HashMap<>();
		final Map<Locale, String> descriptionMap = new HashMap<>();
		final Map<String, byte[]> images = new HashMap<>();

		for (final Entry<String, String> entry : article.getTitle().entrySet()) {
			titleMap.put(LocaleUtils.toLocale(entry.getKey()), entry.getValue());
		}
		for (final Entry<String, String> entry : article.getDescription().entrySet()) {
			descriptionMap.put(LocaleUtils.toLocale(entry.getKey()), entry.getValue());
		}

		String content = article.getContent().get(this.defaultLocale.toString());

		// default language content
		String defaultLocaleId = LanguageUtil.getLanguageId(this.defaultLocale);
		content = LocalizationUtil.updateLocalization(StringPool.BLANK, "static-content", content, defaultLocaleId, defaultLocaleId, true, true);

		// other language
		for (final Entry<String, String> entry : article.getContent().entrySet()) {
			final String localeId = LanguageUtil.getLanguageId(LocaleUtils.toLocale(entry.getKey()));
			if (!localeId.equals(defaultLocaleId)) {
				content = LocalizationUtil.updateLocalization(content, "static-content", entry.getValue(), localeId, defaultLocaleId, true, true);
			}
		}

		final JournalArticle ja = JournalArticleLocalServiceUtil.addArticle(userId, groupId, 0, 0, 0, // folderId, classNameId, classPK,
				article.getName(), // articleId,
				true, // autoArticleId,
				JournalArticleConstants.VERSION_DEFAULT, titleMap, descriptionMap, content, "general", // type,
				null, // structureId
				null, // templateId,
				StringPool.BLANK, // layoutUuid,
				1, 1, 1970, 0, 0, // displayDateMonth, displayDateDay, displayDateYear, displayDateHour, displayDateMinute,
				0, 0, 0, 0, 0, true, // expirationDateMonth, expirationDateDay, expirationDateYear, expirationDateHour, expirationDateMinute, neverExpire,
				0, 0, 0, 0, 0, true, // reviewDateMonth, reviewDateDay, reviewDateYear, reviewDateHour, reviewDateMinute, neverReview,
				true, // indexable,
				false, StringPool.BLANK, null, // smallImage, smallImageURL, smallImageFile,
				images, StringPool.BLANK, // images, articleURL,
				serviceContext);

		return ja;
	}


	/**
	 * Clear application article
	 *
	 * @param groupId
	 *            The group id whose all articles should be removed
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	protected static void clearArticles(final long groupId) throws Exception {
		final List<JournalArticle> articles = JournalArticleLocalServiceUtil.getArticles(groupId);
		for (final JournalArticle article : articles) {
			JournalArticleLocalServiceUtil.deleteJournalArticle(article);
		}
	}


	/**
	 * @param layoutCompanyId
	 *            The company id to update roles
	 * @param resourcePrimKey
	 *            the PK of the layout
	 * @param resourceGroupId
	 *            the group ID of the layout
	 * @param roleId
	 *            "siteMember"
	 * @param actionIds
	 *            "VIEW, CUSTOMIZE"
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	protected void updateRolePermission(final long layoutCompanyId, final String resourcePrimKey, final long resourceGroupId, final long roleId, final String[] actionIds) throws Exception {
		final Map<Long, String[]> roleIdsToActionIds = new HashMap<>();
		this.logger.debug("actions: " + StringUtils.join(actionIds, ','));
		roleIdsToActionIds.put(Long.valueOf(roleId), actionIds);
		this.logger.debug("update permissions: " + layoutCompanyId + "," + resourcePrimKey + "," + roleId + "," + Arrays.toString(actionIds));
		ResourcePermissionLocalServiceUtil.removeResourcePermission(layoutCompanyId, Layout.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, resourcePrimKey, roleId, ActionKeys.VIEW);
		ResourcePermissionLocalServiceUtil.setResourcePermissions(layoutCompanyId, Layout.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, resourcePrimKey, roleId, actionIds);
	}

}
