/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.mvc.spring.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.portlet.PortletPreferences;
import javax.portlet.ReadOnlyException;

import org.ow2.weblab.portlet.bean.ApplicationBean;
import org.ow2.weblab.portlet.bean.JournalArticleBean;
import org.ow2.weblab.portlet.bean.LayoutBean;
import org.ow2.weblab.portlet.bean.LayoutRolePermissionBean;
import org.ow2.weblab.portlet.bean.PortletInLayoutBean;
import org.ow2.weblab.portlet.bean.RoleBean;
import org.ow2.weblab.portlet.bean.UserBean;
import org.ow2.weblab.portlet.constants.Roles;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.GroupConstants;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.LayoutConstants;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.OrganizationConstants;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.RoleConstants;
import com.liferay.portal.model.Theme;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.service.LayoutSetLocalServiceUtil;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.PortletPreferencesLocalServiceUtil;
import com.liferay.portal.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ThemeLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portal.util.PortletKeys;
import com.liferay.portlet.journal.model.JournalArticle;

/**
 * To setup a Liferay application.
 * Also enables some simplified action to the administrator.
 *
 * @author emilienbondu, ymombrun
 */
@Service
public class ApplicationService extends PortalService {


	/**
	 * The map of layout per id
	 */
	protected final Map<String, Layout> layoutsMap = new HashMap<>();


	/**
	 * The map of articles per id
	 */
	protected final Map<String, JournalArticle> articlesMap = new HashMap<>();


	/**
	 * Whether or not to force reinstallation of the application
	 */
	protected final boolean forceOrganisationReinstall;


	/**
	 * Whether or not to force reinstallation of the layouts at each start (only really needed in case of redeployment)
	 */
	protected final boolean forceLayoutReinstall;


	/**
	 * Whether or not to force reinstallation of the user and roles at each start (only really needed in case of error)
	 */
	protected final boolean forceUserAndRoleReinstall;


	/**
	 * The default constructor using en_US as default locale
	 */
	public ApplicationService() {
		this("en_US", true, false, false);
	}


	/**
	 * @param locale
	 *            The default local to be used when creating user, and configuring layouts
	 * @param forceLayoutReinstall
	 *            Whether or not to force reinstallation of the layouts (should be true most of the time)
	 * @param forceUserAndRoleReinstall
	 *            Whether or not to force reinstallation of the users and roles (should be false most of the time)
	 * @param forceOrganisationReinstall
	 *            Whether or not to force reinstallation of the application (should be false most of the time)
	 */
	public ApplicationService(final String locale, final boolean forceLayoutReinstall, final boolean forceUserAndRoleReinstall, final boolean forceOrganisationReinstall) {
		super(locale);
		this.forceLayoutReinstall = forceLayoutReinstall;
		this.forceUserAndRoleReinstall = forceUserAndRoleReinstall;
		this.forceOrganisationReinstall = forceOrganisationReinstall;
	}


	/**
	 * To setup an application. Create a corresponding organisation, layouts, roles and set permissions.
	 *
	 * @param application
	 *            The application bean describing the application to create
	 * @throws PortalException
	 *             If there is an issue with default user access
	 * @throws SystemException
	 *             In case of other portal specific issue
	 */
	public void doApplicationSetup(final ApplicationBean application) throws PortalException, SystemException {
		final long companyId = PortalUtil.getDefaultCompanyId();
		final long defaultUserId = UserLocalServiceUtil.getDefaultUserId(companyId);

		this.logger.debug("Setting up " + application.getName() + " organisation...");
		try {
			this.setupOrganizationWithLayouts(companyId, defaultUserId, application);
		} catch (final Exception e) {
			this.logger.error("An error occurs while setting up " + application.getName() + " organization:" + e.getMessage(), e);
		}
		this.logger.debug("Setting up " + application.getName() + " Organisation... OK");

		// continuing application creation
		this.logger.debug("Setting up roles...");
		try {
			this.setupRoles(companyId, defaultUserId, application);
		} catch (final Exception e) {
			this.logger.error("An error occurs while setting up roles:" + e.getMessage(), e);
		}
		this.logger.debug("Setting up roles... OK");
		this.logger.debug("Setting up users...");
		try {
			this.setupUsers(companyId, application);
		} catch (final Exception e) {
			this.logger.error("An error occurs while setting up users:" + e.getMessage(), e);
		}

		this.logger.debug("Setting up users... OK");
		this.logger.debug("Setting up permissions...");

		try {
			this.setupLayoutPermissions(companyId, application);
		} catch (final Exception e) {
			this.logger.error("An error occurs while setting up permissions :" + e.getMessage(), e);
		}
		this.logger.debug("Setting up permissions... OK");
	}


	/**
	 *
	 * @param user
	 *            The user bean describing the user to create
	 * @param application
	 *            The application bean describing the application
	 * @return The created user
	 * @throws Exception
	 *             If anything wrong occurs at user creation
	 */
	public User addUser(final UserBean user, final ApplicationBean application) throws Exception {
		final long companyId = PortalUtil.getDefaultCompanyId();
		final List<String> roleNames = user.getRoles();
		if (CollectionUtils.isEmpty(roleNames)) {
			this.logger.error("user MUST have at least one role " + user.getFirstName() + " " + user.getLastName());
			return null;
		}
		long[] roleIds = null;
		final ArrayList<Long> ids = new ArrayList<>();
		for (final String roleName : roleNames) {
			final Role requestedRole = RoleLocalServiceUtil.getRole(companyId, roleName);
			if (requestedRole != null) {
				ids.add(Long.valueOf(requestedRole.getRoleId()));
			}
		}
		roleIds = new long[ids.size()];
		for (int i = 0; i < ids.size(); i++) {
			roleIds[i] = ids.get(i).longValue();
		}

		return this.addUser(companyId, user.getFirstName() + user.getLastName(), user.getFirstName(), user.getLastName(), true, roleIds, application.getName(), user.getEmail());
	}


	/**
	 * @param group
	 *            The group from which all private and public pages should be removed
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	protected void deleteLayouts(final Group group) throws Exception {
		final ServiceContext serviceContext = new ServiceContext();
		serviceContext.setAddGuestPermissions(true);

		for (final Layout layout : LayoutLocalServiceUtil.getLayouts(group.getGroupId(), false)) {
			this.deleteLayout(serviceContext, layout);
		}
		for (final Layout layout : LayoutLocalServiceUtil.getLayouts(group.getGroupId(), true)) {
			this.deleteLayout(serviceContext, layout);
		}
		if (LayoutLocalServiceUtil.hasLayouts(group, true) || LayoutLocalServiceUtil.hasLayouts(group, false)) {
			this.logger.warn("Not all layouts of group " + group.getGroupId() + " have been removed.");
		}
	}


	private void deleteLayout(final ServiceContext serviceContext, final Layout layout) {
	    
	    if (layout != null) {

    		try {
    			// Deletes the layout, its child layouts, and its associated resources.
        		// Call parameters (http://www.liferay.com/fr/community/forums/-/message_boards/message/31699727)
    			LayoutLocalServiceUtil.deleteLayout(layout, true, serviceContext);
    		} catch (final Throwable e) {
    			
    			this.logger.trace("An error occurred removing layout " + layout.getName() + ". Will retry.", e);
    			
        		try {
        			// Deletes only the layout from the database. Also notifies the appropriate model listeners.
        			LayoutLocalServiceUtil.deleteLayout(layout);
        		} catch (final Throwable e2) {
        			this.logger.warn("An error occurred removing layout " + layout.getName() + ".", e2);
        		}
    		}
	    	
	    } else {
	        this.logger.trace("Ignoring NULL layout deletion for serviceContext: " + serviceContext);
	    }
	}


	/**
	 * Setup the organization with layouts of the application
	 *
	 * @param companyId
	 *            The id of the company, used to retrieve the group
	 * @param defaultUserId
	 *            The default userId, seem not to be used
	 * @param application
	 *            The application bean describing the pages to be created
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	protected void setupOrganizationWithLayouts(final long companyId, final long defaultUserId, final ApplicationBean application) throws Exception {
		final long userId = defaultUserId;
		final long parentOrganisationId = OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID;
		final String type = OrganizationConstants.TYPE_REGULAR_ORGANIZATION;
		final boolean site = false;
		final long regionId = 0;
		final long countryId = 0;
		final int statusId = GetterUtil.getInteger(PropsUtil.get("sql.data.com.liferay.portal.model.ListType.organization.status"));
		final String comments = null;
		final ServiceContext serviceContext = new ServiceContext();
		serviceContext.setAddGuestPermissions(true);

		Organization organisation;
		try {
			organisation = OrganizationLocalServiceUtil.getOrganization(companyId, application.getName());
			this.logger.debug("Organisation with name " + organisation.getName() + " already exist.");
		} catch (final Exception e) {
			this.logger.trace(application.getName() + " organisation does not exists (not an issue if we are at first start).", e);
			organisation = null;
		}
		boolean deleted = false;
		if (organisation != null && this.forceOrganisationReinstall) {
			try {
				organisation = OrganizationLocalServiceUtil.deleteOrganization(organisation.getOrganizationId());
				deleted = true;
			} catch (final Exception e) {
				this.logger.trace(application.getName() + " organisation cannot be removed.", e);
			}
		}

		if (organisation == null || deleted) {
			this.logger.debug("(Re?)creating the organisation " + application.getName() + ".");
			organisation = OrganizationLocalServiceUtil.addOrganization(userId, parentOrganisationId, application.getName(), type, regionId, countryId, statusId, comments, site, serviceContext);
		}

		final Group group = GroupLocalServiceUtil.getGroup(companyId, GroupConstants.GUEST);
		if (this.forceLayoutReinstall) {
			PortalService.clearArticles(group.getGroupId());
			this.deleteLayouts(group);
			this.updateTheme(application, group);
			this.setupJournalArticles(companyId, defaultUserId, application);

			this.logger.debug("Setting " + application.getName() + " layouts");
			for (final LayoutBean layoutBean : application.getLayouts()) {
				final Layout layout = this.createLayout(companyId, group, layoutBean, LayoutConstants.DEFAULT_PARENT_LAYOUT_ID);
				if ((layout != null) && !layoutBean.getInnerLayouts().isEmpty()) {
					for (final LayoutBean innerLayoutBean : layoutBean.getInnerLayouts()) {
						this.createLayout(companyId, group, innerLayoutBean, layout.getLayoutId());
					}
				}
			}
		}
	}


	private Layout createLayout(final long companyId, final Group group, final LayoutBean layoutBean, final long parentLayoutId) throws SystemException, ReadOnlyException, PortalException {
		Layout createdLayout = null;
		try {
			createdLayout = this.addLayout(group, layoutBean, parentLayoutId);
		} catch (final Exception e) {
			this.logger.error("Exception while creating following layout " + layoutBean.getName(), e);
		}

		if (createdLayout != null) {
			this.layoutsMap.put(layoutBean.getName(), createdLayout);

			for (final PortletInLayoutBean portlet : layoutBean.getPortlets()) {
				if (!portlet.getPortletId().equalsIgnoreCase(PortletKeys.JOURNAL_CONTENT)) {
					this.addPortletId(companyId, createdLayout, portlet.getPortletId(), portlet.getColumn(), portlet.getPreferences());
				} else {
					for (final Entry<String, String> pref : portlet.getPreferences().entrySet()) {

						if (pref.getKey().equalsIgnoreCase("articleName")) {
							final Layout layout = this.layoutsMap.get(layoutBean.getName());
							final JournalArticle article = this.articlesMap.get(pref.getValue());
							// add a content display portlet
							// The column id and position (last 2 parameters below) will depend on your layout template
							final String journalPortletId = this.addPortletId(companyId, layout, PortletKeys.JOURNAL_CONTENT, portlet.getColumn(), portlet.getPreferences());

							final long ownerId = PortletKeys.PREFS_OWNER_ID_DEFAULT;
							final int ownerType = PortletKeys.PREFS_OWNER_TYPE_LAYOUT;

							// Retrieve the portlet preferences for the journal portlet instance just created
							final PortletPreferences prefs = PortletPreferencesLocalServiceUtil.getPreferences(companyId, ownerId, ownerType, layout.getPlid(), journalPortletId);

							// set desired article id for content display portlet
							prefs.setValue("articleId", article.getArticleId());
							prefs.setValue("groupId", String.valueOf(GroupLocalServiceUtil.getGroup(companyId, GroupConstants.GUEST).getGroupId()));

							// update the portlet preferences
							PortletPreferencesLocalServiceUtil.updatePreferences(ownerId, ownerType, layout.getPlid(), journalPortletId, prefs);

							// update the modified layout
							LayoutLocalServiceUtil.updateLayout(layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(), layout.getTypeSettings());
						}
					}
				}
			}
		} else {
			this.logger.warn("Layout " + layoutBean.getName() + " was not created correctly. Corresponding liferay layout is null.");
		}
		return createdLayout;
	}


	/**
	 * To add articles
	 *
	 * @param companyId
	 *            The id of the company, used to retrieve the group
	 * @param defaultUserId
	 *            The default userId, seem not to be used
	 * @param application
	 *            The application bean describing the pages to be created
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	protected void setupJournalArticles(final long companyId, final long defaultUserId, final ApplicationBean application) throws Exception {
		for (final JournalArticleBean articleBean : application.getJournalArticles()) {
			this.articlesMap.put(articleBean.getName(), this.addArticle(companyId, defaultUserId, articleBean));
		}
	}


	/**
	 * Update theme for this application, organisation and group
	 *
	 * @param application
	 *            The application bean describing the pages to be created
	 * @param group
	 *            The name of the group to apply look and feel
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	protected void updateTheme(final ApplicationBean application, final Group group) throws Exception {
		this.logger.debug("Setting " + application.getName() + " theme on layouts");

		// is there a default theme id defined for this application ?
		if (StringUtils.hasText(application.getDefaultThemeId())) {
			if (this.logger.isDebugEnabled()) {
				this.logger.debug("Apply the theme with default ID: '" + application.getDefaultThemeId() + "'");
			}
			LayoutSetLocalServiceUtil.updateLookAndFeel(group.getGroupId(), false, application.getDefaultThemeId(), "01", "", false);
			LayoutSetLocalServiceUtil.updateLookAndFeel(group.getGroupId(), true, application.getDefaultThemeId(), "01", "", false);
		} else {
			boolean found = false;
			final List<Theme> themes = ThemeLocalServiceUtil.getWARThemes();
			for (final Theme theme : themes) {
				if (theme.getName().toUpperCase().contains(application.getName().toUpperCase())) {
					LayoutSetLocalServiceUtil.updateLookAndFeel(group.getGroupId(), false, theme.getThemeId(), "01", "", false);
					LayoutSetLocalServiceUtil.updateLookAndFeel(group.getGroupId(), true, theme.getThemeId(), "01", "", false);
					found = true;
					break;
				}
			}

			if (!found) {
				if (this.logger.isWarnEnabled()) {
					this.logger.warn("No default theme defined and " + application.getName() + " theme not found. Liferay default theme will be used");
				}
			}
		}
	}


	/**
	 * Setup the application roles
	 *
	 * @param companyId
	 *            The id of the company, used to retrieve the group
	 * @param defaultUserId
	 *            The default userId, seem not to be used
	 * @param application
	 *            The application bean describing the pages to be created
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	protected void setupRoles(final long companyId, final long defaultUserId, final ApplicationBean application) throws Exception {
		for (final RoleBean role : application.getRoles()) {
			Role fetchedRole = null;
			boolean deleted = false;
			try {
				fetchedRole = RoleLocalServiceUtil.fetchRole(companyId, role.getName());
				if (fetchedRole != null && this.forceUserAndRoleReinstall) {
					RoleLocalServiceUtil.deleteRole(fetchedRole);
					deleted = true;
				}
			} catch (final Exception e) {
				this.logger.warn("Role '" + role.getName() + "' may not exists or is not deletable", e);
			}
			if ((fetchedRole == null) || deleted) {
				this.logger.debug("Creating '" + role.getName() + "' role");
				fetchedRole = RoleLocalServiceUtil.addRole(defaultUserId, null, 0, role.getName(), null, null, RoleConstants.TYPE_REGULAR, null, null);
			}

			if (fetchedRole != null && (role.getName().equalsIgnoreCase(Roles.WEBLAB_ADMINISTRATOR) || this.forceUserAndRoleReinstall)) {
				// organization roles
				ResourcePermissionLocalServiceUtil.setResourcePermissions(companyId, Organization.class.getName(), ResourceConstants.SCOPE_GROUP_TEMPLATE,
						String.valueOf(GroupConstants.DEFAULT_PARENT_GROUP_ID), fetchedRole.getRoleId(), new String[] { ActionKeys.ASSIGN_MEMBERS, ActionKeys.MANAGE_USERS, ActionKeys.VIEW });

				ResourcePermissionLocalServiceUtil.setResourcePermissions(companyId, User.class.getName(), ResourceConstants.SCOPE_GROUP_TEMPLATE,
						String.valueOf(GroupConstants.DEFAULT_PARENT_GROUP_ID), fetchedRole.getRoleId(),
						new String[] { ActionKeys.UPDATE, ActionKeys.DELETE, ActionKeys.IMPERSONATE, ActionKeys.VIEW });
			}
		}
	}


	/**
	 * Setup application users
	 *
	 * @param companyId
	 *            The id of the company, used to retrieve the group
	 * @param application
	 *            The application bean describing the pages to be created
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	protected void setupUsers(final long companyId, final ApplicationBean application) throws Exception {

		for (final UserBean user : application.getUsers()) {

			if (!this.forceUserAndRoleReinstall) {
				User existingUser = UserLocalServiceUtil.fetchUserByEmailAddress(companyId, user.getEmail());
				if (existingUser != null) {
					this.logger.debug("User " + user.getFirstName() + " already exist. Do not force recreation.");
					continue;
				}
			}

			final List<String> roleNames = user.getRoles();

			if (CollectionUtils.isEmpty(roleNames)) {
				this.logger.warn("This user do not have any role: must not happen! " + user.getFirstName() + " " + user.getLastName());
			}

			final ArrayList<Long> ids = new ArrayList<>();
			for (final String roleName : roleNames) {
				final Role role = RoleLocalServiceUtil.getRole(companyId, roleName);
				if (role != null) {
					ids.add(Long.valueOf(role.getRoleId()));
				} else {
					this.logger.warn("Could not find liferay role corresponding to this role name " + roleName);
				}
			}
			final long[] roleIds = new long[ids.size()];
			for (int i = 0; i < ids.size(); i++) {
				roleIds[i] = ids.get(i).longValue();
			}

			this.addUser(companyId, user.getFirstName(), user.getFirstName(), user.getLastName(), true, roleIds, application.getName(), user.getEmail());
		}
	}


	/**
	 * Setup application layouts permissions
	 *
	 * @param companyId
	 *            The id of the company, used to retrieve the group
	 * @param application
	 *            The application bean describing the pages to be created
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	protected void setupLayoutPermissions(final long companyId, final ApplicationBean application) throws Exception {
		for (final LayoutRolePermissionBean layoutPermission : application.getLayoutsRolesPermissions()) {
			final Role role = RoleLocalServiceUtil.getRole(companyId, layoutPermission.getRole());
			if (role == null) {
				this.logger.warn("Unable to setup layout permission for " + layoutPermission.getLayout() + ". Role is null.");
				continue;
			}
			final Layout layout = this.layoutsMap.get(layoutPermission.getLayout());
			if (layout == null) {
				this.logger.warn("Unable to setup layout permission for " + layoutPermission.getLayout() + ". Layout is null.");
				continue;
			}
			try {
				this.logger.debug("Add permission for role " + role.getName() + " on " + layout.getName() + ".");
				this.updateRolePermission(layout.getCompanyId(), String.valueOf(layout.getPrimaryKey()), layout.getGroupId(), role.getRoleId(), layoutPermission.getActions());
			} catch (final Exception e) {
				this.logger.error("Unable to update permission:" + e.getMessage(), e);
			}
		}
	}


}
