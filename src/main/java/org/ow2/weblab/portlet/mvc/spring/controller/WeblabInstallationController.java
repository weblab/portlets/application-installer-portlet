/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.mvc.spring.controller;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.portlet.bean.ApplicationBean;
import org.ow2.weblab.portlet.bean.UserBean;
import org.ow2.weblab.portlet.constants.Roles;
import org.ow2.weblab.portlet.mvc.spring.service.ApplicationService;
import org.ow2.weblab.portlet.mvc.spring.validator.UserValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * User management portlet controller based on the Spring MVC Portlet framework.
 * Process request for the VIEW portlet mode.
 *
 * @author emilienbondu, ymombrun
 */
@Controller(value = "installationController")
@RequestMapping(value = "VIEW")
@SessionAttributes({ "userBean" })
public class WeblabInstallationController {


	/**
	 * The logger used inside the class
	 */
	protected Log logger = LogFactory.getLog(this.getClass());


	/**
	 * The bean defining the application to be created
	 */
	protected ApplicationBean application;


	/**
	 * The service in charge of the creation of the users, roles, pages, layouts...
	 */
	protected ApplicationService applicationService;


	/**
	 * Create a new instance of controller managing a specific application with a specific application service
	 *
	 * @param application
	 *            {@link ApplicationBean} application managed by this controller
	 * @param applicationService
	 *            {@link ApplicationService} service used to manage this application
	 * @throws RuntimeException
	 *             If any error occurred while checking the installation
	 */
	public WeblabInstallationController(final ApplicationBean application, final ApplicationService applicationService) throws RuntimeException {
		this.application = application;
		this.applicationService = applicationService;
		try {
			this.applicationService.doApplicationSetup(this.application);
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}


	/**
	 * @param binder
	 *            The default binder to be enriched with a user validator
	 */
	@InitBinder("userBean")
	protected void initBinder(final WebDataBinder binder) {
		binder.setValidator(new UserValidator());
	}


	// #############################################################################
	// render mapping methods #
	// #############################################################################


	/**
	 * @param model
	 *            The model to be enriched
	 * @param request
	 *            The request that is used to check the role of the user trying to display the portlet
	 * @return the view
	 */
	@RenderMapping()
	public ModelAndView showView(final ModelMap model, final RenderRequest request) {
		if (!model.containsAttribute("userBean")) {
			model.addAttribute("userBean", new UserBean());
		}

		model.addAttribute("isAdmin", Boolean.valueOf(request.isUserInRole(Roles.LIFERAY_ADMINISTRATOR) || request.isUserInRole(Roles.WEBLAB_ADMINISTRATOR)));

		final ModelAndView mav = new ModelAndView("view");
		mav.addAllObjects(model);
		return mav;
	}


	// #############################################################################
	// action mapping methods #
	// #############################################################################


	/**
	 * @param request
	 *            The request that could be used to retrieve additional information
	 * @param response
	 *            The response, used for the redirection to default page
	 * @throws Exception
	 *             If anything wrong occurs
	 */
	@ActionMapping("do_reset_org")
	public void resetPortal(final ActionRequest request, final ActionResponse response) throws Exception {
		this.applicationService.doApplicationSetup(this.application);
		response.sendRedirect("/");
	}


	/**
	 * @param user
	 *            The description of the user to create
	 * @param bindingResult
	 *            The result of the for used to create the user (contains error if any)
	 * @param model
	 *            The model to be updated
	 * @param request
	 *            The request that could be used to retrieve additional information
	 *
	 * @throws Exception
	 *             If anything wrong occurs creating the user
	 */
	@ActionMapping("add_user")
	public void addUser(@Valid final UserBean user, final BindingResult bindingResult, final ModelMap model, final ActionRequest request) throws Exception {
		if (!bindingResult.hasErrors()) {
			this.applicationService.addUser(user, this.application);
			model.addAttribute("userBean", new UserBean());
		}
	}


}
