<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<portlet:defineObjects />
<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="application_installer" />

<portlet:actionURL name="add_user" var="addUserURL"/>

<div class="taglib-header">
	<h1 class="header-title">
		<span><fmt:message key="adduser.message" /></span>
	</h1>
</div>

<form:form commandName="userBean" method="POST" action="${addUserURL }">


	<table>
	<tbody>
		<tr>
			<td><fmt:message key="adduser.firstname" /></td><td><form:input path="firstName"/></td><td><font style="color:red" ><form:errors path="firstName"/></font></td>
		</tr>
		<tr>
			<td><fmt:message key="adduser.lastname" /></td><td><form:input path="lastName"/></td><td><font style="color:red" ><form:errors path="lastName"/></font></td>
		<tr>
		<tr>
			<td><fmt:message key="adduser.email" /></td><td><form:input path="email"/></td><td><font style="color:red" ><form:errors path="email"/></font></td>
		</tr>
	</tbody>
	</table>
	<br>
	<fmt:message key="adduser.role" />
	<form:checkbox path="roles" value="Utilisateur WebLab" checked="checked"/><fmt:message key="adduser.role.user" />
	<form:checkbox path="roles" value="Superviseur WebLab"/><fmt:message key="adduser.role.supervisor" />
	<form:checkbox path="roles" value="Administrateur WebLab"/><fmt:message key="adduser.role.administrator" />
	<br>
	<br>
	<input type="submit" name="add_user" value="<fmt:message key="user.add" />" />

</form:form>



<c:if test="${isAdmin }">
	<div class="taglib-header">
		<h1 class="header-title">
			<span><fmt:message key="reset.title.message" /></span>
		</h1>
	</div>
	<div id="reset_org" class="portlet-msg-alert">
		<form id="do_reset_org_form" method='POST' action="<portlet:actionURL name="do_reset_org" />">
			<fmt:message key="reset.message" /><br><br>
			<input type="submit" name="search_submit" value="<fmt:message key="reset.button" />" />
		</form>
	</div>
</c:if>
