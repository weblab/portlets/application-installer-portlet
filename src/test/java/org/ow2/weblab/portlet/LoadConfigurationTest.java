/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.portlet.bean.ApplicationBean;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Ensure that provided configuration is valid.
 *
 * @author ymombrun
 */
public class LoadConfigurationTest {


	@Test
	public void testLoadingConfiguration() throws Exception {
		try (final FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("src/main/webapp/WEB-INF/WebLabApplication.xml")) {
			final ApplicationBean appBean = context.getBean("WebLabApplication", ApplicationBean.class);
			Assert.assertNotNull(appBean);
		}
	}

}
